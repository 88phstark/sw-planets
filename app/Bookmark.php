<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $fillable = [
        'user_id', 'planet_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
