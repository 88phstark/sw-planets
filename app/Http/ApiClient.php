<?php

namespace App\Http;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

/**
 * Class ApiClient
 * @package App\Http
 */
class ApiClient
{
    /**
     * @var base_url Base url of remote SWAPI api
     * @var url Api url with resource name included
     * @var resource_name Resource name to get from the remote SWAPI api
     * @var client instance of GuzzleHttp\Client
     */
    private static $api_url = 'https://swapi.co/api/';
    private $url;
    private $resource_name;
    private $client;

    /**
     * ApiClient constructor.
     * @param string $resource_name
     */
    public function __construct(string $resource_name = 'planets')
    {
        $this->resource_name = $resource_name;
        $this->url = self::$api_url . $resource_name;
        $this->client = new Client();
    }

    /**
     * Send Get http request in order to search into given resources
     *
     * @param string $search
     * @param int $page
     * @param string $format
     * @return array|mixed|\Psr\Http\Message\ResponseInterface
     * @throws GuzzleException
     */
    public function search(string $search = '', int $page = 1, string $format = '')
    {
        $url = $this->buildUrl(compact('search', 'page', 'format'));

        //call http get request
        $res = $this->client->request('GET', $url);

        //format & prepare result
        $res = $this->formatResults(json_decode($res->getBody()), $page);

        return $res;
    }

    /**
     * Get a specific given resource from the api
     *
     * @param int $id
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws GuzzleException
     */
    public function get(int $id)
    {
        //call http get request
        $res = $this->client->request('GET', $this->url . '/' . $id);
        $res = json_decode($res->getBody());
        $res->id = $this->resultId($res->url);

        return $res;
    }

    /**
     * buildUrl method : build uri with parameters for http get request
     *
     * @param array $params
     * @return string
     */
    private function buildUrl(array $params)
    {
        $url = $this->url . '?';

        foreach ($params as $key => $value) {

            $url .= sprintf('%s=%s&', $key, $value);
        }

        return $url;
    }

    /**
     * Formats results before sending them to repository
     *
     * @param \stdClass $data
     * @param int $page
     * @return array
     */
    private function formatResults(\stdClass $data, int $page = 1)
    {
        if (!isset($data->results) || empty($data->results)) {
            return [
                'count' => 0,
                'page' => null,
                'previous' => null,
                'next' => null,
                $this->resource_name => [],
            ];
        }

        //Get results ids (not directly given by remote api)
        $results = $data->results;
        foreach ($results as $key => $result) {
            $result->id = $this->resultId($result->url);
            $results[$key] = $result;
        }

        return [
            'count' => $data->count,
            'current_page' => $page,
            'previous' => !empty($data->previous) ? $page - 1 : null,
            'next' => !empty($data->next) ? $page + 1 : null,
            $this->resource_name => $results,
        ];
    }

    /**
     * Get id from result
     *
     * @param $result_url
     * @return int
     */
    private function resultId($result_url)
    {
        $url = str_replace($this->url, '', $result_url);
        $id = (int)str_replace('/', '', $url);

        return $id;
    }
}