<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Bookmark;

class BookmarksController extends Controller
{
    /**
     * BookmarksController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Bookmark::create([
            'user_id' => Auth::id(),
            'planet_id' => request('planet_id'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bookmark $bookmark
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Bookmark $bookmark)
    {
        $bookmark->delete();

        return redirect()->back();
    }
}
