<?php

namespace App\Http\Controllers;

use App\Repositories\PlanetRepository;
use Illuminate\Support\Facades\Auth;

class PlanetsController extends Controller
{
    /**
     * PlanetsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of planets
     *
     * @param string $search
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(string $search = '', int $page = 1)
    {
        //Get lists of bookmarks & planets ids for current user
        $this->userIds();

        $repo = new PlanetRepository();

        $data = $repo->all($search, $page, '');

        return view('planets.index', $data);
    }

    /**
     * Display a listing of planets
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myPlanets()
    {
        //Get lists of bookmarks & planets ids for current user
        $this->userIds();

        $repo = new PlanetRepository();

        $planets = $repo->allByIds(Auth::user()->planets_ids);

        return view('planets.myplanets', compact('planets'));
    }

    /**
     * Redirect post form to correct get http request to index
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function processForm()
    {
        $search = request('search');
        $page = request('page');

        return redirect(sprintf('planets/%s/%s', $search, $page));
    }

    /**
     * Display the specified planet.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        //Get lists of bookmarks & planets ids for current user
        $this->userIds();

        $repo = new PlanetRepository();

        $planet = $repo->show($id);

        return view('planets.show', compact('planet'));
    }

    /**
     * Prepare planets & bookmarks ids lists for current user
     */
    private function userIds()
    {
        $planets_ids = [];
        $bookmarks_ids = [];

        if (!empty(Auth::user()->bookmarks)) {
            foreach (Auth::user()->bookmarks as $bookmark) {
                $planets_ids[$bookmark->id] = $bookmark->planet_id;
                $bookmarks_ids[$bookmark->planet_id] = $bookmark->id;
            }
        }

        Auth::user()->setAttribute('planets_ids', $planets_ids);
        Auth::user()->setAttribute('bookmarks_ids', $bookmarks_ids);
    }
}
