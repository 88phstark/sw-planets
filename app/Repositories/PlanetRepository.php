<?php

namespace App\Repositories;

use App\Http\ApiClient;

class PlanetRepository
{
    /**
     * @var ApiClient
     */
    private $api_client;

    /**
     * PlanetRepository constructor.
     */
    public function __construct()
    {
        $this->api_client = new ApiClient('planets');
    }

    /**
     * Search into api for planets with criterium of name
     *
     * @param string $search
     * @param int $page
     * @param string $format
     * @return array|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all(string $search = '', int $page = 1, $format = '')
    {
        $data = $this->api_client->search($search, $page, $format);

        return $data;
    }

    /**
     * Get multiple planets by id
     *
     * @param array $ids
     * @return array
     */
    public function allByIds(array $ids)
    {
        $planets = [];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $planets[] = $this->show($id);
            }
        }

        return $planets;
    }

    /**
     * Get one specific planet by id
     *
     * @param $id
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show($id)
    {
        $planet = $this->api_client->get($id);

        return $planet;
    }

}