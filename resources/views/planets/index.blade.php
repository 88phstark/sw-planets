@extends('layouts.app')

@section('content')
    <div class="container text-center">
        <h3>Star Wars Planets</h3>
    </div>
    @if (!empty($planets))
        <div class="container">
            <p>{{ sprintf('%d %s found', $count, $count > 1 ? 'planets' : 'planet') }}</p>
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Population</th>
                    <th scope="col">Climate</th>
                    <th scope="col">Terrain</th>
                    <th scope="col"><i class="far fa-thumbs-up"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($planets as $planet)
                    <tr>
                        <th scope="row">
                            <a href="{{ route('planet', $planet->id) }}" class="bade badge-pill">
                                {{ $planet->name }}
                            </a>
                        </th>
                        <td>{{ $planet->population }}</td>
                        <td>{{ $planet->climate }}</td>
                        <td>{{ $planet->terrain }}</td>
                        <td>
                            @if (in_array($planet->id, auth()->user()->planets_ids))
                                <form method="POST" action="/bookmarks/{{ auth()->user()->bookmarks_ids[$planet->id] }}"
                                      id="remove-{{ $planet->id }}-form">
                                    @method('DELETE')
                                    @csrf
                                    <a href="#"
                                       onclick="document.getElementById('remove-{{ $planet->id }}-form').submit();">
                                        <i class="fas fa-star"></i>
                                    </a>
                                </form>
                            @else
                                <form method="POST" action="/bookmarks"
                                      id="add-{{ $planet->id }}-form">
                                    @csrf

                                    <input type="hidden" name="planet_id" value="{{ $planet->id }}"/>
                                    <a href="#"
                                       onclick="document.getElementById('add-{{ $planet->id }}-form').submit();">
                                        <i class="far fa-star"></i>
                                    </a>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <nav>
            <ul class="pagination justify-content-center">
                <li class="page-item {{ $previous ? '' : 'disabled' }}">
                    <a class="page-link"
                       href="{{ $previous ? route('planets.search', ['search' => Request()->search, 'page' => $previous]) : '#' }}"
                       tabindex="-1">Previous</a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">{{ $current_page }}</a></li>
                <li class="page-item {{ $next ? '' : 'disabled' }}">
                    <a class="page-link"
                       href="{{ $next ? route('planets.search', ['search' => Request()->search, 'page' => $next]) : '#' }}">Next</a>
                </li>
            </ul>
        </nav>
    @else
        <div class="container">
            <p>No planet found</p>
        </div>
    @endif
@endsection
