@extends('layouts.app')

@section('title', sprintf('My Favorite Planets'))

@section('content')
    <div class="container text-center">
        <h3>My Favorite Planets</h3>
    </div>
    @if (!empty($planets))
        <div class="container">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Population</th>
                    <th scope="col">Climate</th>
                    <th scope="col">Terrain</th>
                    <th scope="col"><i class="far fa-thumbs-up"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($planets as $planet)
                    <tr>
                        <th scope="row">
                            <a href="{{ route('planet', $planet->id) }}" class="bade badge-pill">
                                {{ $planet->name }}
                            </a>
                        </th>
                        <td>{{ $planet->population }}</td>
                        <td>{{ $planet->climate }}</td>
                        <td>{{ $planet->terrain }}</td>
                        <td>
                            @if (in_array($planet->id, auth()->user()->planets_ids))
                                <form method="POST" action="/bookmarks/{{ auth()->user()->bookmarks_ids[$planet->id] }}"
                                      id="remove-{{ $planet->id }}-form">
                                    @method('DELETE')
                                    @csrf
                                    <a href="#"
                                       onclick="document.getElementById('remove-{{ $planet->id }}-form').submit();">
                                        <i class="fas fa-star"></i>
                                    </a>
                                </form>
                            @else
                                <form method="POST" action="/bookmarks"
                                      id="add-{{ $planet->id }}-form">
                                    @csrf

                                    <input type="hidden" name="planet_id" value="{{ $planet->id }}"/>
                                    <a href="#"
                                       onclick="document.getElementById('add-{{ $planet->id }}-form').submit();">
                                        <i class="far fa-star"></i>
                                    </a>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="container">
            <p>No planet bookmarked at this moment</p>
        </div>
    @endif
@endsection
