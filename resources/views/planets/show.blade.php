@extends('layouts.app')

@section('title', sprintf('Planet %s', $planet->name))

@section('content')
    <div class="container">
        <div class="card text-center">
            <div class="card-header">
                <h5 class="card-title"><span>{{ $planet->name }}</span>
                    @if (in_array($planet->id, auth()->user()->planets_ids))
                        <form method="POST" action="/bookmarks/{{ auth()->user()->bookmarks_ids[$planet->id] }}"
                              id="remove-{{ $planet->id }}-form" class="float-right">
                            @method('DELETE')
                            @csrf
                            <a href="#"
                               onclick="document.getElementById('remove-{{ $planet->id }}-form').submit();">
                                <i class="fas fa-star"></i>
                            </a>
                        </form>
                    @else
                        <form method="POST" action="/bookmarks"
                              id="add-{{ $planet->id }}-form" class="float-right">
                            @csrf

                            <input type="hidden" name="planet_id" value="{{ $planet->id }}"/>
                            <a href="#"
                               onclick="document.getElementById('add-{{ $planet->id }}-form').submit();">
                                <i class="far fa-star"></i>
                            </a>
                        </form>
                    @endif
                </h5>
            </div>
            <div class="card-body">
                <div class="card-text">
                    <dl>
                        <dt>Name</dt>
                        <dd>{{ $planet->name }}</dd>
                        <dt>Population</dt>
                        <dd>{{ $planet->population }}</dd>
                        <dt>Climate</dt>
                        <dd>{{ $planet->climate }}</dd>
                        <dt>Terrain</dt>
                        <dd>{{ $planet->terrain }}</dd>
                        <dt>Surface Water</dt>
                        <dd>{{ $planet->surface_water }}</dd>
                        <dt>Diameter</dt>
                        <dd>{{ $planet->diameter }}</dd>
                        <dt>Gravity</dt>
                        <dd>{{ $planet->gravity }}</dd>
                        <dt>Rotation Period</dt>
                        <dd>{{ $planet->rotation_period }}</dd>
                        <dt>Orbital Period</dt>
                        <dd>{{ $planet->orbital_period }}</dd>
                        <dt></dt>
                    </dl>
                </div>
            </div>
            <div class="card-footer text-muted">
                <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
@endsection