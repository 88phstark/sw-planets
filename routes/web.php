<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PlanetsController@index');

Auth::routes();

Route::get('/planets', 'PlanetsController@index')->name('planets');
Route::get('/planets/{search}', 'PlanetsController@index');
Route::get('/planets/{search}/{page}', 'PlanetsController@index')->name('planets.search');
Route::post('/planets', 'PlanetsController@processForm');

Route::get('/planet/{id}', 'PlanetsController@show')->name('planet');

Route::post('/bookmarks', 'BookmarksController@store');
Route::delete('/bookmarks/{bookmark}', 'BookmarksController@destroy');

Route::get('/user/planets', 'PlanetsController@myPlanets')->name('user.planets');